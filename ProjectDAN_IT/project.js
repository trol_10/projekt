
//Our Services//
function openTab(contentName,tabName) {
    let i, tabscontent, tablinks;
    let tab = document.getElementById(tabName)
    tabscontent = document.getElementsByClassName("service_list");
    for (i = 0; i < tabscontent[0].children.length; i++) {
        tabscontent[0].children[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("service_tab");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(contentName).style.display = "flex";
    tab.className += " active";
}

///Our Amazing Work///


// let images = ["img/Layer%2024.png" ,"img/Layer%2025.png","img/Layer%2026.png","img/Layer%2027.png","img/Layer%2028.png","img/Layer%2029.png","img/Layer%2030.png","img/Layer%2024.png" ,"img/Layer%2025.png","img/Layer%2026.png","img/Layer%2027.png","img/Layer%2028.png","img/Layer%2029.png","img/Layer%2030.png","img/Layer%2024.png" ,"img/Layer%2025.png","img/Layer%2026.png","img/Layer%2027.png","img/Layer%2028.png","img/Layer%2029.png","img/Layer%2030.png"]
//
function loadImages ()
{
    let classNames = ['graphic','webdesign','landingpage','wordpress']
    let images = ["img/Layer%2024.png" ,"img/Layer%2025.png","img/Layer%2026.png","img/Layer%2027.png","img/Layer%2028.png","img/Layer%2029.png","img/Layer%2030.png","img/Layer%2024.png" ,"img/Layer%2025.png","img/Layer%2026.png","img/Layer%2027.png","img/Layer%2028.png","img/Layer%2029.png","img/Layer%2030.png","img/Layer%2024.png" ,"img/Layer%2025.png","img/Layer%2026.png","img/Layer%2027.png","img/Layer%2028.png","img/Layer%2029.png","img/Layer%2030.png"]
    let button  = document.getElementsByClassName('load')
    let imagesList = document.getElementsByClassName('images_list')
    for (let i = 0; i < 12; i++)
    {
        let div = document.createElement('div')
        let img = document.createElement('img')

        // img.src=images[i]
        img.setAttribute('src',images[i])
        div.appendChild(img)
        div.className = classNames[i%4]
        imagesList[0].appendChild(div)
    }
    button[0].style.display='none'

}

/// WORK//
function sortImages (className)
{
    let images = document.getElementsByClassName("images_list")

    for (let i=0; i<images[0].children.length; i++)
    {
        if (className ==='All' )
        {
         images[0].children[i].style.display=''
        }
        else
        {
            if (images[0].children[i].className === className)
            {
                images[0].children[i].style.display=''
            }
            else
            {
                images[0].children[i].style.display='none'
            }
        }

    }
}

///swiper//
function changeImg (id)
{
    let imageName = document.getElementsByClassName('img_first')
    let buttonNames = document.getElementsByClassName('swiper-wrapper')

    for (let i=0; i<buttonNames[0].children.length; i++)
    {

        if (id === 'swiper1')
        {
            if (buttonNames[0].children[i].children[0].src === imageName[0].src)
            {
                imageName[0].src= i===1 ? buttonNames[0].children[4].children[0].src : buttonNames[0].children[i-1].children[0].src
                break
            }
        }

        else if (id === 'swiper6')
        {
            if (buttonNames[0].children[i].children[0].src === imageName[0].src)
            {
                imageName[0].src= i===4 ? buttonNames[0].children[1].children[0].src : buttonNames[0].children[i+1].children[0].src
                break
            }
        }


      else if (id === buttonNames[0].children[i].id)
        {
            imageName[0].src=buttonNames[0].children[i].children[0].src
            break
        }


    }
}

/////Gallery of best image


function openImages ()
{
    let classNames = ['box1','box2','box3','box4']
    let images = ["img/vanglo-house-1%201.png","img/vanglo-house-6%201.png","img/ringve-museum-1%201.png","img/80493541_1281644acb_o%201.png","img/p1_8%201.png","img/vanglo-house-1%202.png","img/3.png","img/7328272788_c5048326de_o%201.png"]
    let button  = document.getElementsByClassName('button_load')
    let imagesList=document.getElementsByClassName('gallery_group')
    for (let i = 0; i < 8; i++)
    {
        let div = document.createElement('div')
        let img = document.createElement('img')

        img.setAttribute('src',images[i])
        div.appendChild(img)
        div.className = classNames[i%4]
        imagesList[0].appendChild(div)
    }
    button[0].style.display='none'

}